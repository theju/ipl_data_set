const Sequelize = require('sequelize');
const connectorToMysqlTable = require('../model/connectorToMysqlTable');

// ___________________ 1st question

function numberOfMatchesPlayes() {
    return new Promise(async function (resolve, reject) {
        let obj = {};
        connectorToMysqlTable.matches.findAll({
            group: ['season'],
            attributes: ['season', [Sequelize.fn('count', Sequelize.col('season')), 'matchPlayed']]
        }).then(result => {
            for (let i = 0; i < result.length; i++) {
                if (result[i].dataValues.season !== 0)
                    obj[result[i].dataValues.season] = (result[i].dataValues.matchPlayed).toString();
                // console.log(result[i].dataValues.season, " matchplayedCount ", result[i].dataValues.seasonCount);
            }
            // console.log(obj);
            // res.json(obj);
            resolve(obj);
        })
    });
}


// ______________________2nd question

function matchesWonPerSeason() {
    return new Promise(async function (resolve, reject) {

        connectorToMysqlTable.matches.findAll({
            group: ['winner', 'season'],
            attributes: ['winner', 'season', [Sequelize.fn('count', Sequelize.col('winner')), 'seasonCount']]
        }).then(function (result) {
            let year = {};
            for (let i = 0; i < result.length; i++) {
                if (result[i].dataValues.season !== 0 && result[i].dataValues.winner !== '') {
                    if (year.hasOwnProperty(result[i].dataValues.winner)) {
                        year[result[i].dataValues.winner][result[i].dataValues.season] = result[i].dataValues.seasonCount;
                    } else {
                        year[result[i].dataValues.winner] = {};
                        year[result[i].dataValues.winner][result[i].dataValues.season] = result[i].dataValues.seasonCount;
                    }
                }
            }
            var years = ["2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017"];
            for (let key in year) {
                var a = Object.keys(year[key]);
                for (var i = 0; i < years.length; i++) {
                    if (!(a.includes(years[i]))) {
                        year[key][years[i]] = 0;
                    }
                }
            }
            var obj1 = {};
            var result = [];
            for (const key in year) {
                obj1 = {};
                if (key !== '') {
                    console.log(key)
                    obj1["name"] = key;
                    obj1["data"] = Object.values(year[key]);
                    result.push(obj1);
                }
            }
            // console.log("year   ",result );
            resolve(result);
        });
    });

}

//_______________________3rd question

function extraRunsConceded() {
    return new Promise(async function (resolve, reject) {
        var obj = {};
        connectorToMysqlTable.deliveries.findAll({
            attributes: ["bowling_team", [Sequelize.fn("SUM", Sequelize.col("extra_runs")), "extraRuns"]],
            group: ["bowling_team"],
            include: [{
                model: connectorToMysqlTable.matches,
                where: {
                    season: 2016
                },
                attributes: ["season"],
            }],
        }).then(result => {
            for (let i = 0; i < result.length; i++) {
                if (obj.hasOwnProperty(result[i].dataValues.bowling_team))
                    obj[result[i].dataValues.bowling_team] += parseInt(result[i].dataValues.extraRuns);
                else
                    obj[result[i].dataValues.bowling_team] = parseInt(result[i].dataValues.extraRuns);
            }
            console.log(obj);
            resolve(obj);
        })
    });

}

// ____________________________4th question 

function economicalRate() {
    return new Promise(async function (resolve, reject) {

        var obj = {};
        connectorToMysqlTable.deliveries.findAll({
            group: ['bowler'],
            attributes: ['bowler', [Sequelize.literal('(SUM(total_runs)) / (COUNT(ball)/6)'), 'eco'],
                [Sequelize.literal('(SUM(total_runs))'), 'total_runs'],
                [Sequelize.literal('(COUNT(ball/6))'), 'ball']
            ],
            include: [{
                model: connectorToMysqlTable.matches,
                where: {
                    season: 2015
                }
            }],
            order: Sequelize.literal('eco ASC'),
            limit: 10
        }).then(result => {
            for (let i = 0; i < result.length; i++) {
                obj[result[i].dataValues.bowler] = result[i].dataValues.eco
            }
            // console.log(obj);
            resolve(obj);
        })
    });
}

module.exports = {
    numberOfMatchesPlayes: numberOfMatchesPlayes,
    matchesWonPerSeason: matchesWonPerSeason,
    extraRunsConceded: extraRunsConceded,
    economicalRate: economicalRate
};