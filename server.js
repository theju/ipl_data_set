const express = require('express');
const calculation = require('./config/calculations');
const ejs = require('ejs');

const db = require('./model/sequelizeSetUp');

db.authenticate()
  .then(() => console.log('database connected'))
  .catch(err => console.log('Error: ' + err));

const app = express();

app.set('view engine', ejs);

app.get('/api/numOfMatchesPlayed', function (req, res) {
  calculation.numberOfMatchesPlayes().then(function (data) {
    res.send(data);
  })
})



app.get("/api/matchesWonPerSeason", function (req, res) {
  calculation.matchesWonPerSeason().then(function (data) {
    res.send(data);
  })
});
app.get("/api/extraRunsConceded", function (req, res) {
  calculation.extraRunsConceded().then(function (data) {
    res.send(data);
  })
});
app.get("/api/economicalRate", function (req, res) {
  calculation.economicalRate().then(function (data) {
    res.send(data);
  })
});


app.get('/api/numOfMatchesPlayedChart', function (req, res) {
  calculation.numberOfMatchesPlayes().then(function (data) {
    res.render(__dirname + '/views/que1Chart.ejs', {
      result: JSON.stringify(data)
    });
  })

});
app.get('/api/matchesWonPerSeasonChart', function (req, res) {
  calculation.matchesWonPerSeason().then(function (data) {
    res.render(__dirname + '/views/que2Chart.ejs', {
      result: JSON.stringify(data)
    });
  })

});

app.get('/api/extraRunsConcededChart', function (req, res) {
  calculation.extraRunsConceded().then(function (data) {
    res.render(__dirname + '/views/que3Chart.ejs', {
      result: JSON.stringify(data)
    });
  })

});

app.get('/api/economicalRateChart', function (req, res) {
  calculation.economicalRate().then(function (data) {
    res.render(__dirname + '/views/que4Chart.ejs', {
      result: JSON.stringify(data)
    });
  })

});

app.listen(3000, console.log('server is running on the port 3000'));